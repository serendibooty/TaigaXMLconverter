import xml.etree.ElementTree as ET

animestatus=["","Watching","Completed","On-Hold","Dropped","Plan to Watch"]
try:
	with open("anime.xml",encoding = "utf8") as f: 
		line = f.readline()
		while(not line.startswith("</meta>")): #read till end of metatag
			line = f.readline()
		library = ET.fromstringlist(["<root>", f.read(), "</root>"]).find("library") #read from <database> and wrap root tag

	root = ET.Element("myanimelist")
	for a in library.iterfind("anime"): #iterate through a generator for <anime> tags
		child = ET.SubElement(root, "anime")
		ET.SubElement(child, "series_animedb_id").text = a.find("id").text
		ET.SubElement(child, "series_title").text = "malID( {} )".format(a.find("id").text)
		ET.SubElement(child, "my_watched_episodes").text = a.find("progress").text
		ET.SubElement(child, "my_start_date").text = a.find("date_start").text
		ET.SubElement(child, "my_finish_date").text = a.find("date_end").text
		ET.SubElement(child, "my_score").text = str(int(int(a.find("score").text)/10))
		ET.SubElement(child, "my_status").text = animestatus[int(a.find("status").text)]
		ET.SubElement(child, "my_times_watched").text = a.find("rewatched_times").text
		ET.SubElement(child, "my_rewatching_ep").text = a.find("rewatching_ep").text
	tree = ET.ElementTree(root)
	tree.write("converted.xml")
except FileNotFoundError:print("anime.xml not found!")
except ET.ParseError:print("ParseError!")
else:print("XML converted successfully")
input("Press enter to exit...")
